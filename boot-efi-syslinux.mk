# Prepare the SYSLINUX UEFI boot option.
#
# This makefile handles the virtual target "boot-efi-isolinux", which
# prepares additional boot equipment over "boot-bios-isolinux" for
# UEFI disk boot and UEFI cdrom boot.
#
# Note that the UEFI disk setup uses a copy of the isolinux menu. This
# is needed since the iso9660 filesystem is not available to the UEFI
# disk boot. UEFI disk boot uses a FAT filesystem that UEFI sees as a
# separate partition, and the cdrom cannot be mounted with a syslinux
# configuration.

WORK = ${BUILDDIR}/efitree
EFIIMG = ${BUILDDIR}/boot/efi.img

ifeq (${ARCH},i386)
SLETYPE = efi32
else ifeq (${ARCH},amd64)
SLETYPE = efi64
endif

EFILOADER = /EFI/BOOT/BOOTX$(subst efi,,${SLETYPE}).efi

# All done in a script, except actual ISO "burning" which is in makeiso.mk
${EFIIMG}:
	env EFIIMG="$@" \
	    MAINDIR="$$(pwd)" \
	    EFILOADER="${EFILOADER}" \
	    WORK=${WORK} \
	    SLETYPE=${SLETYPE} \
	    BUILDDIR=${BUILDDIR} \
	    ISOTREE=${ISOTREE} \
	    VOLI=${VOLI} \
	    INITRD=${INITRD} \
	    scripts/make-efiimg.sh

boot-efi-syslinux: ${EFIIMG}

# Add this into the BOOTOPTIONS
BOOTOPTIONS += boot-efi-syslinux
