#!/bin/bash
#
# This is a helper script for reviewing th F1-F10 pages of the
# syslinux install.

if [ -z "$1" ] ; then
    exec xterm -bg black -fn 10x20 -geometry 80x30 -e $0 f1
    exit 1
fi

DISTNAME=daedalus
DISTVERSION=5.0.3-rc2
BUILD_DATE=$(date +%Y%m%d)
INITRD=/boot/isolinux/initrd.gz
KERNEL=/boot/isolinux/linux

#-- Compile source to deplyment file to stdout
compile() {
    BEEP= \
	BOOTPROMPT=install \
	CONSOLE= \
	BUILD_DATE="${BUILD_DATE}" \
	DEBIAN_VERSION="${DISTNAME} ${DISTVERSION}" \
	DISTNAME="${DISTNAME}" \
	INITRD=${INITRD} \
	KERNEL=${KERNEL} \
	MEDIA_TYPE="ISO" \
	SYSDIR=${ISOLINUXSYSDIR} \
	SYSLINUX_CFG=isolinux.cfg \
	VIDEO_MODE_GTK= \
	VIDEO_MODE="vga=788 nomodeset" \
	X86_KERNEL= envsubst < $1 | syslinux2ansi
}

compile ./boot/isolinux/$1.txt

ul=$(tput smul)
endul=$(tput rmul)

read -p "
Enter <${ul}Number${endul}>, ${ul}N${endul}ext, ${ul}P${endul}revious, ${ul}Q${endul}uit or e${ul}X${endul}it: " NEXT

case $NEXT in
    [QqXx]*)
	exit
	;;
    [Nn]*)
	NEXT=$((${1//[!0-9]/} +1))
	;;
    [Pp]*)
	NEXT=$((${1//[!0-9]/} -1))
	;;
    *)
esac

[ "$(($NEXT +0))" = "$NEXT" ] && NEXT=f$NEXT
if [ -r ./boot/isolinux/${NEXT}.txt ] ; then
    exec $0 ${NEXT}
else
    exec $0 $1
fi

