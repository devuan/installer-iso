#!/bin/zsh
#
# Make an ISO set for a given distribution

set -x

# to avoid perl complains about not configured locales
export LC_ALL=C

SUITE=${1:-daedalus}
ISOSET=${2:-all}
DISTVERSION=${3:-5.0.3-rc2}
: ${SOURCE:=http://pkgmaster.devuan.org/merged}
: ${ARCH:=$(dpkg-architecture -q DEB_HOST_ARCH)}
: ${TARGET:=$SUITE.${ARCH}.fs}

if [ $(id -u) = 0 ] ; then
    alias sudo=""
fi

if [ -n "${http_proxy}" ] ; then
    echo "# Using http_proxy ${http_proxy}" >&2
    PROXY="${http_proxy}"
fi

# helper
function intarget() {
    echo sudo -E chroot $TARGET "$@" >&2
    sudo -E chroot $TARGET "$@"
}

if [ ! -d $TARGET ] || [ -z "$TRIAL" ]  ; then

    if [ -e $TARGET ]; then
	echo "# Clean up prior building..."
	findmnt $TARGET/dev/pts && sudo umount -v $TARGET/dev/pts
	sudo rm -rf $TARGET
	echo
    fi

    echo "# Set up a chroot environment to build in" >&2
    if [ ! -e /usr/share/debootstrap/scripts/$SUITE ] ; then
	echo "MISSING debootstrap script /usr/share/debootstrap/scripts/$SUITE" >&2
	exit 1
    fi

    sudo env http_proxy=${http_proxy} debootstrap \
	 --arch ${ARCH} --variant=minbase \
	 $SUITE $TARGET $SOURCE
    echo
    
    echo "# Configure apt" >&1
    cat <<EOF | sudo tee $TARGET/etc/apt/sources.list > /dev/null
deb $SOURCE $SUITE main contrib non-free non-free-firmware
deb $SOURCE $SUITE main/debian-installer
deb $SOURCE $SUITE contrib/debian-installer
deb $SOURCE $SUITE non-free/debian-installer
deb-src $SOURCE $SUITE main contrib non-free non-free-firmware
EOF

    # Use ceres repo for forked syslinux
    [ -d $TARGET/etc/apt/sources.list.d ] || mkdir -p $TARGET/etc/apt/sources.list.d
    cat <<EOF | sudo tee $TARGET/etc/apt/sources.list.d/ceres.list > /dev/null
deb http://pkgmaster.devuan.org/devuan ceres main
EOF
    [ -d $TARGET/etc/apt/preferences.d ] || mkdir -p $TARGET/etc/apt/preferences.d
    cat <<EOF | sudo tee $TARGET/etc/apt/preferences.d/pinnings > /dev/null
Package: *
Pin: release n=$SUITE
Pin-Priority: 500

Package: *
Pin: release n=ceres
Pin-Priority: -15

Package: systemd*
Pin: release n=*
Pin-Priority: -1
EOF

    cat <<EOF | sudo tee $TARGET/etc/apt/apt.conf.d/02local > /dev/null
$([ -z "${http_proxy}" ] || echo "Acquire::http::proxy \"${http_proxy}\";")
Binary::apt::APT::Keep-Downloaded-Packages "true";
APT::Install-Recommends "0";
APT::Sandbox::User "root";
EOF

    # next apt-get command will try to write to /dev/pts
    echo "# Set up /dev/pts"
    intarget mount -t devpts none /dev/pts

    ## Ensure keyring (excalibur...)
    intarget apt-get install -y --allow-unauthenticated devuan-keyring

    ## Refresh Packages files
    intarget apt-get update

    ARCH="$(dpkg-architecture -q DEB_HOST_ARCH)"
    echo
    echo "# Install required build host packages except grub" >&2
    BUILDPKGSCOMMON=(
	build-essential newlisp
	fakeroot libbogl-dev bf-utf-source dosfstools mtools win32-loader
	tofrodos xorriso zsh debhelper po-debconf wget zstd
	fdisk dpkg-dev gzip busybox-static colorized-logs
    )

    case "$ARCH" in
	amd64)
	    BUILDPKGS=(
		linux-image-amd64 grub-efi-amd64-bin # grub-pc
	    )
	    ;;
	i386)
	    BUILDPKGS=(
		linux-image-686
	    )
	    ;;
	*)
	    echo "Unknown architecture: $ARCH" >&2
	    exit 1
	    ;;
    esac
    case "$SUITE" in
	excalibur)
	    BUILDPKGS=(
		$BUILDPKGS make
	    	isolinux syslinux-common syslinux-utils
		syslinux syslinux-efi
	    )
	    ;;
	*)
	    BUILDPKGS=(
		$BUILDPKGS
	    	isolinux/ceres syslinux-common/ceres syslinux-utils/ceres
		syslinux/ceres syslinux-efi/ceres
	    )
	    ;;
    esac
    echo "============================================" >&2
    intarget apt-get -y install $BUILDPKGSCOMMON $BUILDPKGS 
    echo "====================xxxx====================" >&2

else
    echo "**** Reusing existing $TARGET" >&2
fi

#### Now copy the installer into the build system
echo "# Transfer the installer-iso package" >&2
DATA=(
    ././Makefile ././*.mk ././udeb-sets.mk.tmpl ././scripts ././runbuild.sh
    ././boot ././docs ././needed-characters ././pool
    ././localudebs(N) ././localdebs(N)
)
sudo mkdir $TARGET/installer-iso
sudo rsync -avR $DATA $TARGET/installer-iso/

echo "# Build all" >&2

ENV="SUITE=${SUITE} DISTVERSION=${DISTVERSION} http_proxy='${http_proxy}'"
intarget /bin/bash -c "cd installer-iso && env $ENV ./runbuild.sh $ISOSET"
ERR=$?

if [ $ERR -eq 0 ] && [ -z "$TRIAL" ] ; then
    # Keep the last 5 log collections and add a new one
    for d in ${$(echo content/*/*(Nom))[5,-1]} ; do rm -r $d ; done
    rsync -av $TARGET/installer-iso/content .
fi
[ -z "$TRIAL" ] && sudo umount -v $TARGET/dev/pts
exit $ERR
