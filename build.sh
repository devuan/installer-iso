#!/bin/sh
#
# Perform a full local build including chroot setup
# [ ARCH [ CHROOT [ SUITE [ MAKESET ] ] ] ]

set -e
set -x

HOSTARCH="$(dpkg-architecture -q DEB_HOST_ARCH)"
SUITE=daedalus
#PROXY=
HOST=http://pkgmaster.devuan.org/merged
CHROOT=CHROOT
MAKESET=all

# Override settings with command line parameters
ARCH=${1:-$HOSTARCH}
CHROOT=CHROOT-${1:-$ARCH}
CHROOT=${2:-$CHROOT}
SUITE=${3:-$SUITE}
MAKESET=${4:-$MAKESET}

# Prepare package list for ISO building
INCLUDE="build-essential mawk zsh make wget git newlisp debhelper"
INCLUDE="$INCLUDE po-debconf libbogl-dev bf-utf-source ca-certificates"
INCLUDE="$INCLUDE dosfstools mtools win32-loader tofrodos xorriso"

# Prepre host path for fakeroot
case "$HOSTARCH" in
    amd64)
	LLPH="/usr/lib/x86_64-linux-gnu"
    ;;
    i386)
	LLPH="/usr/lib/i386-linux-gnu"
	;;
    ppc64el)
	LLPH=/usr/lib/powerpc64le-linux-gnu
	;;
    *)
	echo "Unknown fakeroot library host path for $HOSTARCH" >&2
	exit 1
	;;
esac

# Prepre target variations: includes, kernel and path for fakeroot
case "$ARCH" in
    amd64)
	INCLUDE="$INCLUDE isolinux/ceres syslinux-common/ceres syslinux-utils/ceres"
	INCLUDE="$INCLUDE grub-efi-amd64"
	KERNEL="linux-image-amd64 kmod tiny-initramfs"
	LLPT="/usr/lib/x86_64-linux-gnu"
	;;
    i386)
	INCLUDE="$INCLUDE isolinux/ceres syslinux-common/ceres syslinux-utils/ceres"
	INCLUDE="$INCLUDE grub-efi-ia32"
	KERNEL="linux-image-686 kmod tiny-initramfs"
	LLPT="/usr/lib/i386-linux-gnu"
	;;
    ppc64el)
	INCLUDE="$INCLUDE" # grub-efi-??-bin
	KERNEL="linux-image-powerpc64le tiny-initramfs"
	LLPT=/usr/lib/powerpc64le-linux-gnu
	;;
    *)
	echo "Unknown architecture $ARCH" >&2
	exit 1
	;;
esac

############################################################
### Use debootstrap to initialize a CHROOT build "system"

[ -z "$PROXY" ] || PROXYENV=http_proxy="http://$PROXY"

# Setup LD_LIBRARY_PATH for fakeroot/fakechroot using LLPH and LLPT
LLP="$LLPH/libfakeroot:$LLPH/fakechroot"
LLP="$LLP:$LLPT/libfakeroot:$LLPT/fakechroot"
LLP="LD_LIBRARY_PATH=$LLP:$LD_LIBRARY_PATH"

# Patch for debootstrap calling setup_proc_fakechroot
PATH="/usr/sbin:/sbin:$PATH:$(pwd)/scripts"

# Prepare base system
if fakechroot -e debootstrap -- fakeroot -- env "$LLP" $PROXYENV \
	      debootstrap --verbose --variant=fakechroot --no-merged-usr \
	      --include libfakechroot,libfakeroot \
	      --arch $ARCH $SUITE $CHROOT $HOST ; then
    : # debootstrap return 0
else
    R=$?
    echo "debootstrap returns $R"
    exit $R
fi

# (handy aliases)
INCHROOT="fakechroot -e debootstrap fakeroot chroot $CHROOT"
APTGET="env DEBIAN_FRONTEND=noninteractive apt-get"

############################################################
### Set up the CHROOT for installing build system packages

# install the proxy if there is one
if [ -n "$PROXY" ] ; then
    echo "Acquire::http::proxy \"http://$PROXY\";" \
	 > $CHROOT/etc/apt/apt.conf.d/02proxy
fi

# avoid automatic loading of "Recommends" dependencies
echo 'APT::Install-Recommends "0";' \
     > $CHROOT/etc/apt/apt.conf.d/05norecommends

# avoid sandboxing
echo 'APT::Sandbox::User "root";' \
     > $CHROOT/etc/apt/apt.conf.d/01sandbox

# install user root
echo "root:x:0:0:root:/root:/bin/bash" > $CHROOT/etc/passwd
echo "root:*:17575:0:99999:7:::" > $CHROOT/etc/shadow

# install groups that some packages otherwise fail installation for
cat <<EOF > $CHROOT/etc/group
root:x:0:
mail:x:1:
utmp:x:2:
staff:x:3:
crontab:x:4
EOF
cat <<EOF > $CHROOT/etc/gshadow
root:*::
mail:x::
utmp:x::
staff:X::
crontab:x::
EOF

# install some shells; some package(s) needs this file
cat <<EOF > $CHROOT/etc/shells
/bin/sh
/bin/dash
/bin/bash
EOF

# set up the CHROOT sources.list
cat <<EOF > $CHROOT/etc/apt/sources.list
deb $HOST $SUITE main contrib non-free
deb $HOST $SUITE main/debian-installer
deb $HOST $SUITE-security main contrib non-free
deb $HOST $SUITE-security main/debian-installer
deb $HOST $SUITE-updates main contrib non-free
deb $HOST $SUITE-updates main/debian-installer
deb-src $HOST $SUITE main contrib non-free
deb-src $HOST $SUITE-security main contrib non-free
deb-src $HOST $SUITE-updates main contrib non-free
EOF

# Use ceres repo for forked syslinux
[ -d $CHROOT/etc/apt/sources.list.d ] || mkdir -p $CHROOT/etc/apt/sources.list.d
cat <<EOF > $CHROOT/etc/apt/sources.list.d/ceres.list
deb http://pkgmaster.devuan.org/devuan ceres main
EOF
[ -d $CHROOT/etc/apt/preferences.d ] || mkdir -p $CHROOT/etc/apt/preferences.d
cat <<EOF > $CHROOT/etc/apt/preferences.d/pinnings
Package: *
Pin: release n=$SUITE
Pin-Priority: 500

Package: *
Pin: release n=ceres
Pin-Priority: -15

EOF

# Fix access permissions of the prepared files 
$INCHROOT chown root:root /etc/passwd /etc/group /etc/shells
$INCHROOT chown root:root /etc/shadow /etc/gshadow 
$INCHROOT chmod go= /etc/shadow /etc/gshadow
[ -z "$PROXY" ] || $INCHROOT chown root:root /etc/apt/apt.conf.d/02proxy
$INCHROOT chown root:root /etc/apt/apt.conf.d/05norecommends
$INCHROOT chown root:root /etc/apt/apt.conf.d/01sandbox
$INCHROOT chown root:root /etc/apt/sources.list
$INCHROOT chown -R root:root /etc/apt/sources.list.d
$INCHROOT chown -R root:root /etc/apt/preferences.d

############################################################
### Fluff up the ISO building CHROOT system
#
# INCLUDE is the list of all packages except the KERNEL; both
# duly set up for the target architecture

$INCHROOT $APTGET -y install devuan-keyring
$INCHROOT $APTGET -y update
$INCHROOT $APTGET -y install $INCLUDE
$INCHROOT $APTGET -y dist-upgrade
$INCHROOT $APTGET -y install $KERNEL

############################################################
### Add the installer-iso software to CHROOT and make the ISOs

mkdir $CHROOT/installer-iso
rsync -av boot docs localudebs needed-characters pool scripts \
      Makefile *.mk runbuild.sh udeb-sets.mk.tmpl \
      $CHROOT/installer-iso

export SUITE
$INCHROOT installer-iso/runbuild.sh $MAKESET
