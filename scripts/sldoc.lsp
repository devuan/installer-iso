#!/usr/bin/newlisp
#
# This script is a stream filter to creates a text file of the help
# pages f1.txt-f10.txt. It strips all controles, and replaces ^Y with
# horizontal rule

(setf X (pipe))
(process "/bin/sh -c 'syslinux2ansi | ansi2txt'" 0 (X 1))
;(close 0)
(close (X 1))
(setf RULE (dup "=" 70))
(while (setf LINE (read-line (X 0)))
  (if (empty? LINE) (println)
    (= 25 (char (LINE 0))) (println RULE "\n" (1 LINE))
    (println LINE)))
(exit 0)
