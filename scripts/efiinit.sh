#!/bin/sh
#
# This script is intended to be interpreted by busybox sh.

emerg() {
    echo "$*"
    echo "*** Starting emergency shell ..."
    setsid cttyhack sh -i
    true
}

# Function to pass cmdline assignments into environent
cmdenv() {
    for I in $(seq 1 $#) ; do
	[ -z "${1##*=*}" ] || continue
	eval ${1%%=*}="${1#*=}"
	shift
    done
}

mkdir -p /cdrom /target /mnt
for M in proc:/proc devtmpfs:/dev devpts:/dev/pts sysfs:/sys ; do
    mkdir -p ${M##*:}
    mount -t ${M%%:*} ${M%%:*} ${M##*:}
done

if grep -qw EMERG /proc/cmdline ; then
    emerg "== EMERG command shell =="
fi

eval cmdenv $(cat /proc/cmdline)

echo
echo
cat devuan-logo.txt
echo
echo === Devuan boot preamble ===

mount -t ramfs -o size=800M initrd /target || emerg "*** Not enough RAM"

echo "Load modules:"
for M in usb-storage mmc-block iso9660 exfat ; do modprobe -abq $M ; done
for N in $(seq 1 10) ; do
    echo -n " $N"
    lsmod > A
    find /sys/ -name modalias -perm /u=r -print0 | \
	xargs -0 cat | tr '\012' '\000' | \
	xargs -0 modprobe -abq 2>/dev/null
    lsmod > B
    diff -q A B >/dev/null && break
done
echo
rm -f A B

sync

# Wait for the kernel to relax
if grep -qw emerg0 /proc/cmdline ; then
    emerg "== emerg0 command shell =="
fi

# Mount the cdrom, method 1
for I in 1 2 3 ; do
    unpack && break
    [ $? = 2 ] && emerg "*** Unpacking error" && break
    [ $I = 3 ] && emerg "*** failed to mount the cdrom" && break
    sleep $I
done

sync

# remove all unused modules
lsmod | while read M L ; do modprobe -r $M || true ; done 2> /dev/null

if grep -qw emerg /proc/cmdline ; then
    emerg "== emerg command shell =="
fi

exec switch_root /target /init
emerg "*** switch_root failed ***"
exec /bin/sh
