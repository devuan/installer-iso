#!/bin/sh
#
# Try to mount the cdrom and unpack its initrd to /target
set -e

# Patch /target for ventoy ISO access
# 1. Use "losetup" from here (i.e. busybox.static)
# 2. Add udev rules to mark /dev/loop* as cdrom
# 3. Add a debian-installer-startup.d snippet to
#    3a. mount the Ventoy partition $DEV on /mnt, and
#    3b. then set up $SIO as loop device
ventoy_injection() {
    cp /sbin/losetup /target/sbin/
    cat <<EOF > /target/lib/udev/rules.d/82-ventoy-loop.rules
KERNEL=="loop*", ENV{ID_CDROM}="1"
EOF
    cat <<EOF > /target/lib/debian-installer-startup.d/S21ventoy-loop
sleep 1
mount -t exfat $1 /mnt
modprobe loop
losetup -f $2
EOF
}

LOOP=$(losetup -f)
if ! mount -t iso9660 LABEL=$VOLI /cdrom 2>/dev/null ; then
    mount -t iso9660 LABEL=Ventoy /mnt || \
	mount -t exfat LABEL=Ventoy /mnt || exit 1 # Give up!
    DEV="$(sed '/ \/mnt /{s/ .*//;p}d' proc/mounts)"
    echo -n "[Ventoy $DEV]: "
    for ISO in /mnt/*.iso ; do
	losetup -f $ISO || continue
	if mount LABEL=$VOLI /cdrom 2>/dev/null ; then
	    umount /cdrom
	    echo -n "$ISO"
	    mount -t iso9660 $LOOP /cdrom && break
	fi
	ISO=
	losetup -d $LOOP
    done
    echo
    # loop set up or not
    # /cdrom mounted or not
    # /mnt mounted
fi

sync
RET=1
if [ -r "/cdrom${INITRD}" ] ; then
    if gunzip < "/cdrom${INITRD}"  | ( cd /target ; cpio -i ) ; then
	RET=0
    else
	RET=2
    fi
fi
if [ -n "$ISO" ] ; then ventoy_injection "$DEV" "$ISO" ; fi
sync
umount /cdrom 2>/dev/null || true
losetup -d $LOOP 2>/dev/null || true
umount /mnt 2>/dev/null || true
exit $RET
