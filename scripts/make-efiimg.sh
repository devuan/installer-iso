#!/bin/bash
#

############################################################
# Build a trampoline initrd for the UEFI disk boot, and setup the UEFI
# cdrom boot.
#
# The UEFI disk boot is built using the software of the "normal"
# installer initrd but removing various files that are not needed for
# a boot preamble. The preamble has the simple steps:
#
# 1) set up drivers for media (disk) access,
# 2) mount/find the installation media,
# 3) unpack the installer initrd and switch to it.
#
# The first step uses "vdev-tiny" to detect the boot system hardware
# and load the required kernel modules.
#
# The second step firstly tries to mount the installation media
# directly. If that fails it tries to detect a Ventoy disk with the
# installation media and loopback mount that media.
#
# The third step unpacks the installer initrd into a RAM filesystem to
# switch into its /init scripting. The media is then directly
# available to that installer either as a device partition or as
# loopback media.
#
# Note that this is on top of the previously prepared isolinux boot
# setup in ${ISOTREE}/boot/isolinux.
#
# Incoming environment includes:
#   MAINDIR   = pathname to build sources 
#   EFIIIMG   = filename of target efi partition image (not used here)
#   EFILOADER = pathname for UEFI boot loader within boot partition
#   WORK      = full pathname for assigned efi partition work directory
#   SLETYPE   = either "efi32" or "efi64"
#   BUILDDIR  = relative pathname for the build tree in use
#   ISOTREE   = full pathname for iso9660 tree (to be)
#   VOLI      = the media "volid" field (used as mount label)
#   INITRD    = filename of installer initrd

set -e 
set -x
shopt -s nullglob

# relative pathname for the installer initrd build directory
WORKINI=${BUILDDIR}/initrd

# relative pathname for the premble initrd
WORKEFI=${BUILDDIR}/efi-initrd

# Full pathname for the EFI boot loader in the work tree
HDENTRY=${WORK}${EFILOADER}

# Full pathname for EFI bootloader in the iso9660 tree (to be)
# This may be used by EFI implementation that understands iso9660
CDENTRY=${ISOTREE}${EFILOADER}

# Pathname for ISO boot equipment within iso9660 media/partition
ISOBOOTPATH=/boot/isolinux

# Full pathname for the ISO boot equipment within the iso9660 tree (to be)
ISOTREEBOOT=${ISOTREE}${ISOBOOTPATH}

####################
# Preamble initrd preparation in $WORKEFI

mkdir -p $WORKEFI

# Helper function: copy into WORKEFI after ensuring directory
WORKEFI_install() {
    local F=${WORKEFI}$1
    mkdir -p ${F%/*}
    cat > $F
    [ -z "$2" ] || chmod $2 $F
}

# Prepare the WORKEFI root filesystem
for D in cdrom dev etc mnt proc sys target tmp var ; do
    mkdir -p $WORKEFI/$D
done
for D in bin sbin lib lib32 lib64 ; do
    mkdir -p $WORKEFI/usr/$D
    ln -s usr/$D $WORKEFI/$D
done
cp -r $BUILDDIR/initrd/lib/{modules,firmware} $WORKEFI/lib

# + Drop some files to reduce size; firstly all network kernel modules
for D in \
    $WORKEFI/lib/modules/*/kernel/{net,sound} \
    $WORKEFI/lib/modules/*/kernel/drivers/{net,input,hid} \
    $WORKEFI/lib/modules/*/kernel/drivers/scsi/{bfa,qla2xxx,qla4xxx,lpfc} \
    $WORKEFI/lib/modules/*/kernel/drivers/scsi/{be2iscsi,isci,libfc} \
    ; do
    rm -rf $D
done

# + Configure preamble filesystem to allow for up to 15 partitions in
#   loopback images
WORKEFI_install /etc/modprobe.d/loop.conf <<EOF
options loop max_part=15
EOF

# Install busybox-static
dpkg --root=$WORKEFI -i /var/cache/apt/archives/busybox-static_*
chroot $WORKEFI /bin/busybox --install -s /bin

# + Install scripts/efiinit.sh as preamble /init
WORKEFI_install /init 0755 < scripts/efiinit.sh

# + Install scripts/efiunpack.sh as preamble /sbin/unpack with actual
#   VOLI and INITRD hardcoded
envsubst '$VOLI $INITRD' < scripts/efiunpack.sh | \
    WORKEFI_install /sbin/unpack 0755

# + Install boot/devuan-logo.txt as preamble "text splash"
WORKEFI_install /devuan-logo.txt < boot/devuan-logo.txt

##############################
# Bootloader setup. The UEFI boot equipment uses syslinux.efi with a
# selection of modules, and the menu files from the ISO boot
# equipment.
#
# $SLETYPE is efi32 or efi64.
#
# Note that the UEFI boot equipment is set up both in the EFI
# partition and in the iso9660 partition. 

# Helper function to install EFI boot equipment and kernel but not
# initrd.gz into the given directory.
install_efimodules() {
    # Full pathname for installer modules in build host
    local DIR=${1%/*}
    mkdir -p ${DIR}
    case "${SLETYPE}" in
	efi64)
	    local EFIMODDIR=/usr/lib/syslinux/modules/efi64
	    cp /usr/lib/SYSLINUX.EFI/efi64/syslinux.efi $DIR/BOOTX64.EFI
	    cp ${EFIMODDIR}/ldlinux.e64 $DIR
	    ;;
	efi32)
	    local EFIMODDIR=/usr/lib/syslinux/modules/efi32
	    cp /usr/lib/SYSLINUX.EFI/efi32/syslinux.efi $DIR/BOOTIA32.EFI
	    cp ${EFIMODDIR}/ldlinux.e32 $DIR
	    ;;
    esac
    cp ${EFIMODDIR}/libcom32.c32 $DIR
    cp ${EFIMODDIR}/libutil.c32 $DIR
    cp ${EFIMODDIR}/vesamenu.c32 $DIR
    cat <<EOF > ${DIR}/syslinux.cfg
path ${EFILOADER%/*}
say syslinux UEFI bootloader booting devuan
default devuan
label devuan
    config ${ISOBOOTPATH}/syslinux.cfg ${ISOBOOTPATH}
EOF
    # As needed, copy menu and boot kernel
    local MENUDIR=${DIR%${EFILOADER%/*}}${ISOBOOTPATH}
    if [ ! -d ${MENUDIR} ] ; then 
	mkdir -p ${MENUDIR}
	cp ${ISOTREEBOOT}/{syslinux,menu,stdmenu}.cfg ${MENUDIR}/
	cp ${ISOTREEBOOT}/f{1..10}.txt ${MENUDIR}/
	cp ${ISOTREEBOOT}/splash.png ${MENUDIR}/
	cp ${ISOTREEBOOT}/linux ${MENUDIR}/
    fi
}

# Set up EFI boot equipment in EFI partition
install_efimodules ${HDENTRY}

# Install preamble initrd named intird.gz in EFI partition
( cd ${WORKEFI} ; find . | fakeroot cpio -H newc -o | gzip ) \
    > ${WORK}${ISOBOOTPATH}/initrd.gz

# Set up EFI boot equipment in CDROM
install_efimodules ${CDENTRY}

# Install preamble initrd named preamble.gz for isolinux boot
cp ${WORK}${ISOBOOTPATH}/initrd.gz ${ISOTREEBOOT}/preamble.gz

# Patch bootloader menu to use preamble.gz
sed 's/initrd.gz/preamble.gz/' -i ${ISOTREEBOOT}/menu.cfg

####################
# Pack the EFI partition work tree into a small FAT image named $EFIIMG
mkdir -p $(dirname "${EFIIMG}")
SZ=$(( $(du -s ${WORK} | awk '{print $1}') + 2048 ))
mkfs.fat -C "${EFIIMG}" $SZ
DIRS=( $(find ${WORK} -type d -printf "%P\n") )
FILS=( $(find ${WORK} -type f -printf "%P\n") )
for D in ${DIRS[@]} ; do mmd -i "${EFIIMG}" ::/$D ; done
for F in ${FILS[@]} ; do mcopy -i "${EFIIMG}" ${WORK}/$F ::/$F ; done

## See also the final ISO packing in ../makeiso.mk

