# Makefile fragment for preparing an EFI boot into build/${ARCH}/efi
# and installing into
# a FAT disk image = ${ISOTREE}/boot/efi.img
# a GRUB boot config = ${ISOTREE}/boot/grub/${ARCH}/grub.cfg
#
# Note: EFI boot set up is the same for all ISO's within an architecture

# Relative path for the boot loader, used by xorriso
EFIGRUB = /boot/grub
EGTHEME = devuan.theme
EFIGRUBS = grub.cfg font.pf2 efi.img themes/${EGTHEME}

EFIIMG = ${EFIGRUB}/efi.img

${warning ${ISOTREE}${EFIGRUB}/}

# Temporary directory for packaging the boot loader config file
WORK = build/${ARCH}/efi

# Path for the boot loader configuration file, which must be named "grub.cfg"
EFILOAD = /boot/grub
EFILOADCFG = ${WORK}${EFILOAD}/grub.cfg
EFILOADTAR = ${WORK}/grubloader.tar

# Define a work area for boot partition packaging
ifeq (${ARCH},i386)
EFILOADBIN = ${WORK}/bootia32.efi
GRUBMODFMT=i386-efi
else ifeq (${ARCH},amd64)
EFILOADBIN = ${WORK}/bootx64.efi ${WORK}/bootia32.efi
GRUBMODFMT=x86_64-efi
else
$(error "unknown EFI architecture ${ARCH}")
endif

GRUBCOREMOD = search iso9660 configfile normal memdisk tar
GRUBCOREMOD += part_msdos part_gpt fat
# Prepare the grub boot loader in work area
${WORK}/bootia32.efi ${WORK}/bootx64.efi: ${EFILOADTAR} | ${dir ${EFILOADBIN}}
	grub-mkimage -O ${GRUBMODFMT} -m $< -o $@ -p '(memdisk)${EFILOAD}' \
             ${GRUBCOREMOD}

# Prepare the efi grub config which is included in the efi boot bin-file
#.INTERMEDIATE: ${EFILOADCFG} | ${dir ${EFILOADCFG}}
${EFILOADCFG}: ${dir ${EFILOADCFG}}
	echo 'search --file --set=root /.disk/info' > $@
	echo 'set prefix=($$root)${EFIGRUB}' >> $@
	echo 'source $$prefix/${GRUBMODFMT}/grub.cfg' >> $@

#.INTERMEDIATE: ${EFILOADTAR}
${EFILOADTAR}: ROOT = ${word 1,${subst /, ,${EFILOAD}}}
${EFILOADTAR}: ${EFILOADCFG} | ${dir ${EFILOADTAR}}
	tar cf $@ -C ${WORK} ${ROOT}

# Compute FAT size from grubby master rounded up as the size of a
# number of 32k blocks
GRUBBINSZ = $(addprefix +,$(shell stat -c %s ${EFILOADBIN}))
FATSZ = $(shell echo $$(( ( ( ( $(GRUBBINSZ) ) / 32768 + 2 ) * 32) )) )

# Pick grub font
${ISOTREE}${EFIGRUB}/font.pf2: /usr/share/grub/ascii.pf2
	@cp $< $@

# Pack the boot loader into a FAT partition in the work area
${ISOTREE}${EFIGRUB}/efi.img: ${EFILOADBIN} | ${ISOTREE}${EFIGRUB}/
	/sbin/mkfs.msdos -v -C "$@" ${FATSZ}
	mmd -i "$@" ::efi
	mmd -i "$@" ::efi/boot
	mcopy -i "$@" ${EFILOADBIN} "::efi/boot"

# Install an EFI/ tree into the ISO as well, for virtualbox
${ISOTREE}/EFI: ${EFILOADBIN} ${EFILOADTAR}
	mkdir -p $@/boot
	cp ${EFILOADBIN} $@/boot
	tar xf ${EFILOADTAR} -C $@

# Generic rules for populating target ${EFIGRUB} from templates
${ISOTREE}${EFIGRUB}/themes/${EGTHEME}: | ${ISOTREE}${EFIGRUB}/themes/
${ISOTREE}${EFIGRUB}/themes/${EGTHEME}: boot/grub/${EGTHEME}.tmpl
	env KERNEL=${KERNEL} INITRD=${INITRD} VIDEO_MODE="vga=788 nomodeset" \
	    envsubst '$$KERNEL $$INITRD $$VIDEO_MODE $$DISTNAME' < $< > $@

${ISOTREE}${EFIGRUB}/%: boot/grub/%.tmpl | ${ISOTREE}${EFIGRUB}/
	env KERNEL=${KERNEL} INITRD=${INITRD} VIDEO_MODE="vga=788 nomodeset" \
	    envsubst '$$KERNEL $$INITRD $$VIDEO_MODE $$DISTNAME' < $< > $@

# Handle grub modules (.mod and .lst files)
GRUBMODSRCDIR = /usr/lib/grub/${GRUBMODFMT}
GRUBMODDSTDIR = ${ISOTREE}${EFIGRUB}/${GRUBMODFMT}

GRUBMOD_LOAD = ${patsubst %.mod,%,${filter part_%.mod,${GRUBMODS}}}

# grub module files to install
GRUBMODS =  ${sort ${notdir ${shell echo ${GRUBMODSRCDIR}/*.mod}}}
GRUBMODSKIP = ${GRUBCOREMOD}
GRUBMODSKIP += affs afs afs_be befs befs_be minix nilfs2 sfs zfs zfsinfo
GRUBMODSKIP += example_functional_test functional_test hello
GRUBMOD_INSTALL = ${filter-out ${addsuffix .mod,${GRUBMODSKIP}},${GRUBMODS}}
GRUBMOD_INSTALL += ${sort ${notdir ${shell echo ${GRUBMODSRCDIR}/*.lst}}}

# install a grub .lst file
${GRUBMODDSTDIR}/%.lst: ${GRUBMODSRCDIR}/%.lst | ${GRUBMODDSTDIR}/
	@cp $< $@

# install a grub .mod file
${GRUBMODDSTDIR}/%.mod: ${GRUBMODSRCDIR}/%.mod | ${GRUBMODDSTDIR}/
	@cp $< $@

# prepare a grub.cfg file for module dir
${GRUBMODDSTDIR}/grub.cfg: ${GRUBMODDSTDIR}/
	for i in ${GRUBMOD_LOAD} ; do echo "insmod $${i%.mod}" ; done > $@
	echo 'source $$prefix/grub.cfg' >> $@

# EFI boot with grub needs a FAT and grub modules in the output tree
.PHONY: boot-efi-grub
boot-efi-grub: ${addprefix ${ISOTREE}${EFIGRUB}/,${EFIGRUBS}}
boot-efi-grub: ${ISOTREE}/EFI
boot-efi-grub: ${addprefix ${GRUBMODDSTDIR}/,${GRUBMOD_INSTALL}}
boot-efi-grub: ${GRUBMODDSTDIR}/grub.cfg
boot-efi-grub: ${ISOTREE}${INITRD} ${ISOTREE}${KERNEL}

# Install the boot option
BOOTOPTIONS += boot-efi-grub

$(warning GRUBMODSRCDIR = ${GRUBMODSRCDIR})
$(warning wildcard /* = $(wildcard /*))
$(warning echo /* = $(shell echo /*))
$(warning ls ${GRUBMODSRCDIR} = $(shell ls ${GRUBMODSRCDIR}))
$(warning GRUBMOD_INSTALL = ${GRUBMOD_INSTALL})
$(warning GRUBMOD_LOAD = ${GRUBMOD_LOAD})
