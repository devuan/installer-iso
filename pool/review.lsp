#!/usr/bin/env newlisp
#
# This script reviews a package list file, expanding it to include
# all dependencies, and documenting a "reason" for all the packages:
#   implied = dependency from some other package
#   virtual = dependency, but provided by some package
#   origin = not a dependency
#
# The review uses a package description library in ome or more directories
# named as library-* where each package description is a single file that
# is named by the package it describes.
#
# See library.lsp as a script that creates a library from given Packages
# files.
# [-r] <file> [ -- <package>... ]

;; Setup handler to exit immediately upon an INT signal (i.e. ^C).
(signal 2 (fn (x) (exit 0)))

;; Function to log to stderr.
(define (stderr)
  (write-line 2 (join (map string (args)))))

;; Process the commandline arguments except the first 2 which are:
;; ("newlisp" "./review.lsp")
(let ((it nil) (ARGV (2 (main-args))))
  ;; Handle any optional initial [-r]
  (if (setf it (match '("-r" *) ARGV))
      (begin (setf ARGV (it 0)) (setf USE_RECOMMENDS true)))
  ;; Handle the list file and any additional packages
  (if (setf it (match '(* "--" *) ARGV))
      (set 'LISTFILE (it 0 -1) 'ADDONS (it 1))
    (set 'LISTFILE (ARGV -1) 'ADDONS '()))
  )

;; Determine the host architecture in Debian terms.
(setf ARCH ((exec "dpkg-architecture -qDEB_HOST_ARCH") 0))

;; Ensure that a LISTFILE was given, otherwise complain and exit with error.
(unless (file? LISTFILE)
  (stderr "Usage: [-r] <listfile> [ -- <package>* ]" )
  (exit 1))

;; Declare the DEPS hashtable to map each available package name to
;; its list of declared dependencies. Each dependency is either a
;; package name or a string of package names divided by vertical bars
;; (and optional whitespace). If "-r" was given, then DEPS will also
;; hold the declared "Recommends" for available packages.
(define DEPS:DEPS nil)

;; Declare the BASE hashtable to map regular expression patterns (for
;; package names that seemingly include upstream version code) to
;; available matching package names. This table is used as fallback
;; for supporting an otherwise unsupported dependency by using a later
;; upstream version.
(define BASE:BASE nil)

;; Declare the PICK table to map regular expression patterns to the
;; identified picks of matching packages.
;; Eg (PICK "libc([0-9.]+)") = ("libc6")
(define PICK:PICK nil)

;; Declare the PROV hashtable to map package names to those that may 
;; to the packages that are declared to provide those names.
(define PROV:PROV nil)

;; Declare the SKIP hashtable to enumerate package names to skip.
(define SKIP:SKIP nil)

;; Function to "generalize" a package name by replacing any
;; (seemingly) version code substring with the version code
;; recognition pattern. This "base-name" is thus a regex pattern that
;; would match to all actual packages that concern different versions
;; of the same upstream.
(define (base-name P)
  (replace "[0-9.]+" (copy P) "([0-9.]+)" 0))

;; Function to form a list of the version numbers in a version string.
(define (version P)
  (or (find-all "([0-9]+)" P (int $1 0 10) 0) 0))

; Function to compare two version number lists and return true when
; equal or x is the higher version.
(define (higher-version x y)
  (if (null? x) (null? y) (null? y) true
      (= (x 0) (y 0)) (higher-version (1 x) (1 y))
      (> (x 0) (y 0))))

; Function to compare two version code strings ad return true when
; equal or x is the higher version.
(define (version-order x y)
  (higher-version (version x) (version y)))

;; Function to add the implementation option P to the base name B and
;; keep (BASE B) in version order.
(define (save-base B P)
  (BASE B (sort (cons P (or (BASE B) '())) version-order)))

;; Function to set up initial DEPS and BASE tables for package P. This
;; also sets P as the "the current package" PKG.
(define (save-package P)
  (setf PKG P) ; set up "the current package"
  (DEPS P (list)) ; prepare for dependencies
  (when (find "[0-9]" P 0) ; when it contains a number then add it to BASE
    (save-base (base-name P) P)))

;; Function to add packages PL as being provided by the current
;; package PKG. The list of providers is partially ordered to ensure
;; that the actual so named package, if available, is first on the
;; list.
(define (save-provides PL) ; PKG
  (dolist (P (map trim (parse (replace "\\([^)]+\\)" PL "" 0) ",")))
    (PROV P (if (= P PKG) (append (list PKG) (or (PROV P) '()))
              (append (or (PROV P) '()) (list PKG))))))

;; Function to process a (partial) dependency line to extend DEPS for
;; the current package PKG. Version codes and ":any" suffixes are
;; removed, but dependecy options are kept as such, with vertical-bar
;; separation between the options.
(define (save-depends DL) ; PKG
  (DEPS PKG
        (append (or (DEPS PKG) '())
                (map (fn (D) (replace ":any$" D "" 0))
                     (map trim
                          (parse (replace "\\([^)]+\\)" DL "" 0) ","))))))

;; Function add package P as a PICK for base name B.
(define (add-pick B P)
  (println "# add-pick " B " " P)
  (unless (member P (or (PICK B) '()))
    (if (PICK B) (push P (PICK B) -1) (PICK B (list P)))))

;; Process all library-* and security-* directories of package meta
;; files to build upu DEPS, BASE and PROV tables
(stderr (format "Loading (library|security)-%s-*/* files" ARCH))
(stderr "USE_RECOMMENDS = " USE_RECOMMENDS)
(dolist (LIB (directory "." (format "^(library|security)-%s" ARCH)))
  (dolist (PKG (directory LIB "^[^.]"))
    (dolist (LINE (parse (read-file (format "%s/%s" LIB PKG)) "\n"))
      (when (regex "(^[^:]+): (.*)" LINE 0)
        (case $1
          ("Package" (save-package $2) (save-provides PKG))
          ("Provides" (save-provides $2))
          ("Depends" (save-depends $2))
          ("Pre-Depends" (save-depends $2))
          ("Recommends" (when USE_RECOMMENDS (save-depends $2)))
          (true ))))))

;; Process the PROV table to set up pure virtuals as depending on
;; available implementors.
(dolist (PX (PROV))
  (println (string "# provided " PX))
  (setf PKG (PX 0))
  (if (!= PKG (PX 1 -1))
      (save-depends (join (PX 1) " | "))
    (PROV PKG nil)))

;; Load the SKIP table, if any
(dolist (f (filter file? (parse (or (env "SKIP") "") ":")))
  (dolist (P (clean (fn (x) (or (empty? x) (find "#" P)))
                    (parse (read-file f) "\n")))
    (unless (SKIP P) (SKIP P P))))

;; Now process stdin package list
(setf ADDED '()) ; For tracking additions to IMPLIED

;; Declare the ORIG table for the original packages mentioned in the
;; LISTFILE or as ADDONS
(define ORIG:ORIG nil)

;; Declare the IMPLIED table for packages implied as dependencies.
(define IMPLIED:IMPLIED nil)

;; Declare the CHOOSEN table for chosen packages. (CHOOSEN P)
;; identifies the actual package that is chosen when P is wanted. Here
;; P may be a virtual package, or an optional dependency phrase.
(define CHOOSEN:CHOOSEN nil)

(setf COUNT 0)

;; Function to add X to ADDED and make some noise about it
(define (add-added X)
  (println (format "# add-added %s" (join (map string (args)))))
  (when (= X "php8.1-xsl") (when (> (inc COUNT) 10) (exit 1)))
  (push X ADDED -1))

;; Function to add P as package implied by D, and then add its
;; dependencies into ADDED for "recursive closure"
(define (add-implied P D)
  (println "# add-implied " P " " D)
  (if (list? D) nil
    (find "|" D) (add-added D "add-implied:1 " D)
    (member P (or (IMPLIED D) '())) nil
    (nil? (DEPS D)) (stderr "# unavailable " D )
    (begin (if (IMPLIED D) (push P (IMPLIED D) -1) (IMPLIED D (list P)))
           (add-added D "add-implied:2 " D))
    ))

;; Function to add R to choices for P (with log detail X)
(define (add-choice P R X)
  (println "# add-choice " P " " R " " X)
  (let ((PC (CHOOSEN P)) (LR (list R)))
    (CHOOSEN P (if (null? PC) LR (member R PC) PC (append PC LR)))
    (add-added P "add-choice " P)
    (map (curry add-implied P) (or (DEPS P) '()))
    P))

;; Function to chose package P without upstream versioning
(define (analyse-simple P)
  (add-choice P P "analyse-simple"))

;; Function to chose package P with base name expression B
(define (analyse-base B P)
  (let ((BL (BASE B)))
    (if (null? BL) (println (string "# discard: " P " " B))
      (add-pick B P)
      (add-choice P P "analyse-base"))))

(stderr "Processing")

;; Here the LISTFILE lines plus the ADDONS are processed. All input
;; pacakage names are added to the ORIG table, and then analysed for
;; dependencies.
;;
;; A package name that contains digits is assumed to possibly be named
;; including an upstream version code in the name. 
;; potentially replaceable by a different upstream version of "the
;; same" package. Such package names are analysed via "analyse-base"
;; 
;; Otherwise, if mentioned in the DEPS table, it is analysed via
;; "analyse-simple".
(dolist (P (extend (parse (read-file LISTFILE) "\n") ADDONS))
  (unless (or (empty? P) (starts-with (trim P) "#") (SKIP P))
    (ORIG (trim P) P)
    (if (find "[0-9]" P 0) (analyse-base (base-name P) P)
      (nil? (DEPS P)) (println "# unknown: " P)
      (analyse-simple P))))

;; Function to determine choosen provider for package P, if noted.
(define (has-provider P)
  (exists (fn (x) (and (!= x P) (CHOOSEN x))) (or (PROV P) '())))

;; Function to determine if a package is available in the repo
(define (is-available P)
  (list? (DEPS P)))

;; Function to determine the contextual "first choice" in given
;; package list. PL might be a list of alternatives to chose from;
;; each alternative has provider(s) to investigate for any existing
;; choice
(define (add-first-choice PL)
  (if (exists (fn (P) (CHOOSEN P)) PL) $it
    (if (exists is-available (append (filter has-provider PL) PL))
        (add-choice $it (join PL " | ") (string "add-first-choice:1 " I)))))

;; Declare the MANUAL hashtable that contains the manual choices
(define MANUAL:MANUAL nil)

;; Function to process options PL for base list BL
(define (confirm-choice PL BL)
  (stderr "Confirm choice " PL " for " BL)
  (let ((RL '()))
    (dolist (P PL)
      (cond ((IMPLIED P)
             (stderr P " is implied by " (IMPLIED P))
             (push P RL -1))
            ((member P BL)
             (stderr P " is manual choice")
             (push P RL -1))
            ((null? BL)
             (println "# " P " is UNAVAILABLE manual choice!"))
            ((> P (BL 0))
             (println "# " P " is UNAVAILABLE manual (newer) choice!")
             (stderr P " is UNAVAILABLE manual choice (newer?)")
             (push (BL 0) RL -1))
            (true
             (println "# " P " is UNAVAILABLE manual (older) choice!")
             (stderr P " is UNAVAILABLE manual choice (older?)")
             (push (BL -1) RL -1))))
    (unless RL (setf RL PL))
    (let ((X (read-line)))
      (when (= "y" X) (setf X ""))
      (MANUAL (string PL) (if (empty? X) RL (parse X " "))))))

(define (manual-choice PL BL)
  (if (MANUAL (string PL)) $it (confirm-choice PL BL)))

(define (ssort X) (if (list? X) (sort X) X))

;; Process all implied packages for recursiveness

(while ADDED
  (while ADDED
    (letn ((I (pop ADDED))
           (P (add-first-choice
               (map (fn (x) (replace ":any$" x "" 0))
                    (map trim (parse I "|"))))))
      (when (or (find "|" I) (!= I P))
        (println (string "# choice " P " for " I " by " (ssort (IMPLIED I))))
        (when (and P (IMPLIED I))
          (map (curry add-implied P) (IMPLIED I))))))
  (dolist (PX (PICK))
    (letn ((B (PX 0)) (BL (BASE B)))
      (unless (= (PX 1 0) (BL 0))
        (let ((CL (manual-choice (PX 1) BL)))
          (dolist (P (difference (PX 1) CL)) (CHOOSEN P nil))
          (dolist (P CL)
            (unless (or (null? P) (CHOOSEN P))
              (add-added P "processing " P)))))))
  ) ; end while ADDED

# Now report any unfulfilled virtual dependencies

(println "##### Choosen ###########")

(define (print-result P)
  (println (join (map string (args)) " "))
  (unless (empty? P) (println P)))

; Process the CHOOSEN table with a one-liner comment as to why:
; 1. "# dropped" P is dropped because it's missing in the library
; 2. "# origin" P is an input and not implied
; 3. "# origin" P is an input and implied and has an alternative provider
; 4. "# shadowed" P is implied but has an alternative provider
; 5. "# choosen" P is implied and first choice among non-chosen other
; 6. "# implied" P is implied and without alternative provider
; Note that the package is then NOT included for cases 1 and 4, while
; it is included (printed on the subsequent line) for all other cases.
(dolist (PX (CHOOSEN))
  (let ((P (PX 0)) (PS nil) (it nil))
    (if (nil? (DEPS P)) (print-result "" "# dropped" P)
      (null? (IMPLIED P)) (print-result P "# origin" P)
      (PROV P)
      (if (setf it (exists (fn (x) (and (!= x P) (CHOOSEN x))) (PROV P)))
          (if (ORIG P)
              (print-result P "# origin" P "for" (ssort (PROV P))
                            "implied by" (ssort (IMPLIED P)))
            (print-result "" "# shadowed" P "by" it "for" (ssort (PROV P))
                          "implied" (ssort (IMPLIED))))
        (print-result P "# choosen" P "for"
                      (ssort (PROV P)) "supported by" (ssort (IMPLIED P))))
      (print-result P "# implied" P "by" (ssort (IMPLIED P))))))

(exit 0)
