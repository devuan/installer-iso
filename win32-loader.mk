# Makefile fragment for preparing a win32 loader
#

# Where to place it
WIN32BASE = # ${patsubst %/,%,${dir ${KERNEL}}}
WIN32DSTDIR = ${ISOTREE}${WIN32BASE}

${warning ${WIN32DSTDIR}/}

WIN32SRCDIR = /usr/share/win32
WIN32LDR = setup.exe g2ldr g2ldr.mbr win32-loader.ini

# for g2ldr and g2ldr.mbr
${WIN32DSTDIR}/%: ${WIN32SRCDIR}/win32-loader/%
	cp $< $@

${WIN32DSTDIR}/setup.exe: ${WIN32SRCDIR}/win32-loader.exe
	cp $< $@

${WIN32DSTDIR}/win32-loader.ini: boot/win32-loader/win32-loader.ini.tmpl
	env -i KERNEL=${KERNEL} INITRD=${INITRD} ARCH=${ARCH} \
	    envsubst < boot/win32-loader/win32-loader.ini.tmpl | todos > $@

.PHONY: win32-loader
win32-loader: ${addprefix ${WIN32DSTDIR}/,${WIN32LDR}}

BOOTOPTIONS += win32-loader
