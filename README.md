Devuan Installer ISO
====================

This project is tailored for building the Devuan installation ISOs,
which include the i386 and amd64 architectures, with a collection of ISOs
built for each:

* netinstall - installation fully over the network
* server - ~CD size ISO for server installation, with or without network
* desktop - 4G ISO for desktop installation, also with or without network
* cd2 - package pool desktop add-on for server install with xfce4
* cd3 - package pool desktop add-on for server install with more
* cd4 - package pool desktop add-on for server install with even more
* pool1 - package pool of the 5,000 top votes from Devuan popcon
* pool2 - package pool of the 15,000 top votes from Devuan popcon

The `pool` Directory
--------------------

The `pool` directory contains "seed" files for creating the package
lists for the various ISOs. The first build step for an ISO includes
processing the seed files by recursively following up on all
dependencies and recommended packages so as to generate the actual and
complete package list for the target ISO.

The first build step further sets up subdirectories of package
description snippets in the library-$ARCH-$SECTION directories. The
package description snippets are cutouts from the build host's
Packages files made by splitting them up into separate files for the
individual package descriptions, named by the package name.

The build host must be set up with the target sources and it must be
duly updated for the target ISO building.

The local package description library uses several optional source
points that are overlayed on a simple assumption of version priority.
Overall it uses the following source list points; the first is
required, and the rest are optional:

 1. $CODENAME main main/debian-installer contrib non-free
 3. $CODENAME-security main main/debian-installer
 4. $CODENAME-updates main main/debian-installer
 5. $CODENAME-proposed-updates main main/debian-installer

See the `pool/Makefile` for detils, and use "make -C pool reallyclean"
to clean it up.

How to Build an ISO
-------------------

ISOs are only built for the current (aka host) architecture. Use:

    $ ./build-sudo.sh $SUITE $ISO $VERSION

with

* SUITE being one of ascii, beowulf, chimaera or daedalus

* ISO being one of netinstall, server, desktop, cd2, cd3, pool1 or
  pool2, or the special token `all` to build all of them.

* VERSION being the your desired release version code, e.g. 3.a for
  building a beowulf ISO. This version code gets imprinted into the
  ISO.

Note that `build-sudo.sh` is currently dependent on the `zsh` shell being
installed.

Build dependencies beyond a base install required to complete the process:

    # apt install bash debootstrap dpkg-dev rsync sudo zsh

The user doing the build will need to be able to invoke sudo.

Using APT Proxy
---------------

If you use an apt proxy (like for example `apt-cacher-ng`) you should
export the `http_proxy` variable before building.

Example:

```
export http_proxy="http://localhost:3142/"
```

Including local udebs
---------------------

Locally built udebs for the installer may be added to the appropriate
subdirectory of `localudebs/` for the target architecture.

* `localudebs/i686/` are for i386 udebs
* `localudebs/x86_64/` are for amd64 udebs

Any such a udeb package will be added to the installer and override
any downloadable package of the same name regardless of package
versions.


Including local debs
--------------------

Locally built packages may be added to the installer, to be present
during installation as a local repository. Such packages should be
added into the appropriate subdirectory of `localdebs/` for the target
architecture.

* `localdebs/i686/` are for i386 packages
* `localdebs/x86_64/` are for amd64 packages

The packages will be included as a repository directory named
`/localudebs` on the ISO, and the installer will transiently include
this as a trusted repository during the installation.

Note that the `file:/media/cdrom/localdebs` repository is just as an
alternative package repository and that by default the highest version
available is the installation candidate. Thus, the `localdebs`
packages are default installation candidates only when their versions
are the highest available.


Help?
-----

On occasion, things don't just work. This typically means that the
package collection (which is downloaded on demand) has changed in some
way that results in an inconsistency between package descriptions and
downloaded packages. The master switch for recovery is then a
`reallyclean` of the build area, and an `update + dist-upgrade` of the
build host, possibly followed by a `reboot`, esp if the kernel version
has changed. For example:

    $ make reallyclean
    $ ./build-sudo.sh beowulf netinstall 3.a

