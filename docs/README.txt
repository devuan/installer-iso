
                   Devuan GNU/Linux 5.0 "daedalus" *

            Welcome to the reliable world of Devuan GNU/Linux

   This disc is one of several installation options for the Devuan 
   GNU+Linux [0] distribution. Devuan offers an extensive collection of 
   original and modified Debian as well as Devuan-specific packages. It 
   is a complete Operating System (OS) for your computer. And it is free  
   as in 'freedom'.

   CONTENTS:
     * Introduction
     * Disc Choices
     * Installation
     * Getting Software
     * Devuan Derivatives
     * Report a Bug
     * Devuan Resources
     
   [0]: https://files.devuan.org/devuan_daedalus/

   Introduction
   ============

   An Operating System (OS) is a set of programs that provide an
   interface to the computer's hardware. Resource, device, power and
   memory management belong to the OS. The core of the OS in charge of
   operating the circuitry and managing peripherals is called the
   kernel. Devuan uses the Linux kernel. Most of the basic operating
   system tools come from the GNU project; hence the name GNU+Linux.
   
   Devuan is available for various kinds of computers, like PC
   compatible hardware (i386 and amd64), ppc64el and ARM targets
   (armel, armhf, arm64) such as the allwinner flavour of SBCs and
   some ASUS chromebooks.

   Disc Choices
   ============
     * netinstall
     * server (4 CD set)
     * desktop   
     * desktop-live
     * minimal-live

   Proprietary firmware is automatically installed on systems if
   needed. To avoid installation of non-free firmware, you must choose
   one of the Expert install options and you must also select a
   mirror.

   Installation
   ============

   Before you start, please read the Daedalus 5.0 Release Notes [0],
   and the installer's help text pages [1].

   You can install Devuan GNU+Linux either as a dual (or multiple)
   boot alongside your current OS or as the only OS on your computer.

   An Installation Guide is included on this disc (English version)
   docs/install-devuan.html and also online.

   You can start the installation program easily by booting your
   computer with CD/DVD or from USB. Note that some very old or very
   new systems may not support this.

   [0]: Release_notes_daedalus_5.0.txt
   [1]: BOOTHELP.txt

   Getting Additional Software
   ===========================

   After installing or upgrading, Devuan's packaging system can use
   CDs, DVDs, local collections or networked servers (FTP, HTTP) to
   automatically install software from .deb packages. This is done
   preferably with the 'apt' or 'aptitude' programs.

   You can install packages from the commandline using apt-get. For
   example, if you want to install the packages 'openssh-client' and
   'xlennart', you can give the command:

       apt-get install openssh-client xlennart

   Note that you don't have to enter the complete path or the '.deb'
   extension. Apt will figure this out itself.

   Or use aptitude for a full screen interactive selection of
   available Devuan packages.

   Software can also be installed using the Synaptic graphical
   interface.

   Devuan Derivatives
   ==================

   The default desktop provided by classic installer-iso images
   shouldn't be considered the only way to use Devuan on the desktop.
   A growing number of derivative distributions have already adopted
   Devuan as a base OS. When considering Devuan, we do recommend
   taking derivatives into consideration. They harness the power of
   our base distribution by targeting specific usage. This is exactly
   what we mean to achieve with Devuan. A list of derivative distros
   is kept at: https://www.devuan.org/os/devuan-distros
   
   You are free to create and re-distribute CDs/DVDs of the Devuan
   GNU+Linux Operating System as well as respins like these.

   Report a Bug
   ============

   This is an official release of the Devuan system. Please report any
   bugs you find to the Devuan Bug Tracking System at
   https://bugs.devuan.org.

   If you're reporting bugs against this disc or the installation
   system, please also mention the version of this disc; this can be
   found in the file /.disk/info.

   Devuan Resources
   ================

   Learn more about Devuan, Linux and Libre Software

     * The Devuan homepage [1]
     * The Dev1Galaxy web forum [2]
     * Community Communication Channels [3]
     * Twitter @DevuanOrg [4]
     * The Linux Documentation Project [5]
     * General Linux homepage [6]

   [1]: https://devuan.org/
   [2]: https://dev1galaxy.org
   [3]: https://devuan.org/os/community
   [4]: https://twitter.com/DevuanOrg
   [5]: https://www.tldp.org/
   [6]: https://www.linux.org/
