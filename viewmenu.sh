#!/bin/bash
#

DISTNAME=daedalus
DISTVERSION=5.0.3-rc2
BUILD_DATE=$(date +%Y%m%d)
INITRD=/boot/isolinux/initrd.gz
KERNEL=/boot/isolinux/linux
SYSDIR=/boot/isolinux

#-- Compile source to deplyment file to stdout
compile() {
    BEEP= \
	BOOTPROMPT=install \
	CONSOLE= \
	BUILD_DATE="${BUILD_DATE}" \
	DEBIAN_VERSION="${DISTNAME} ${DISTVERSION}" \
	DISTNAME="${DISTNAME}" \
	INITRD=${INITRD} \
	KERNEL=${KERNEL} \
	MEDIA_TYPE="ISO" \
	SYSDIR=${SYSDIR} \
	SYSLINUX_CFG=syslinux.cfg \
	VIDEO_MODE_GTK= \
	VIDEO_MODE="vga=788 nomodeset" \
	X86_KERNEL= envsubst < $1
}

# Create a DOS filesystem for syslinux menu explorations
IMG=$(mktemp bootimage-XXX.raw)
{
    dd if=/dev/zero of=$IMG bs=100M count=0 seek=1
    cat <<EOF | sfdisk $IMG
label:dos

2048 - 0x0e *
EOF
    mkfs.fat --offset 2048 $IMG
    DOSIMG=$IMG@@$((2048*512))
} >& /dev/null
cleanup() {
    echo $DOSIMG
    rm -f $IMG
}
trap "cleanup" 0

# Install the boot menu into the DOS filesystem
installfile() {
    local F=${2-${1##*/}}
    #echo "$1 => $DOSDIR/$F"
    compile $1 | mcopy -i $DOSIMG - $DOSDIR/$F
}
copyfile() {
    local F=${2-${1##*/}}
    mcopy -i $DOSIMG $1 $DOSDIR/$F
}
DOSDIR=::${SYSDIR}
for D in $(echo ${DOSDIR#::}| tr / ' ') ; do
    P="$P/$D"
    mmd -i $DOSIMG ::$P
done
INSTALL=(
    boot/isolinux/stdmenu.cfg
    boot/isolinux/menu.cfg
    boot/isolinux/syslinux.cfg
    boot/isolinux/f*.txt
)
SPLASH=boot/isolinux/pics/devuan-daedalus-bootscreen-iso-640x480.png
copyfile $SPLASH splash.png

MODDIR=
COPY=(
    /usr/lib/syslinux/modules/bios/vesamenu.c32
    /usr/lib/syslinux/modules/bios/ldlinux.c32
    /usr/lib/syslinux/modules/bios/libcom32.c32
    /usr/lib/syslinux/modules/bios/libutil.c32
)
for F in ${INSTALL[@]} ; do installfile $F ; done
for F in ${COPY[@]} ; do copyfile $F ; done
dd conv=notrunc bs=440 if=/usr/lib/syslinux/mbr/mbr.bin of=$IMG 2>/dev/null
syslinux -t ${DOSIMG#*@@} -d ${DOSDIR#::}/ -i $IMG
qemu-system-x86_64 -drive file=$IMG,media=disk,format=raw
