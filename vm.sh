#!/bin/bash
#
# QEMU setup for trial VM

VARIANT=(
    "legacy boot, iso image as ATA harddisk"
    "UEFI boot  , iso image as ATA harddisk"
    "legacy boot, iso image as CDROM"
    "UEFI boot  , iso image as CDROM"
    "legacy boot, iso image as USB 3 disk"
    "UEFI boot  , iso image as USB 3 disk"
    "legacy boot, iso image as nvme disk"
    "UEFI boot  , iso image as nvme disk"
    "legacy boot, iso image as mmc disk"
    "UEFI boot  , iso image as mmc disk"
    "legacy boot, iso image as SCSI disk on selected host device"
    "UEFI boot  , iso image as SCSI disk on selected host device"
)

FUSEFILE=/usr/sbin/fusefile
[ -x "$FUSEFILE" ] || FUSEFILE=

M=$(( $1 +0 ))

if [ ${M} -ge 1 ] && [ ${M} -le ${#VARIANT} ] ; then
    REPLY=$1
else
    select V in "${VARIANT[@]}" ; do
	[ -z "$V" ] || break
    done
fi
[ -z "$REPLY" ] && exit 0

if [ -z "$FILE" ] ; then
    FILE=$(echo excalibur.amd64.fs/installer-iso/*.iso)
    if [ -z "$ISOTYPE" ] ; then
	ISOTYPE=${FILE##*-}
	ISOTYPE=${ISOTYPE%.iso}
    fi
fi

MACHINE="-M pc,accel=kvm -cpu host -vga qxl"
case "${ISOTYPE}" in
    amd64) QEMU=qemu-system-x86_64 ;;
    i386) QEMU=qemu-system-i386 ;;
    ia32) QEMU=qemu-system-x86_64 ; MACHINE="-M pc,accel=kvm -cpu Penryn -vga qxl" ;;
    *) echo "Unknown ISO type '${ISOTYPE}'" ; exit 1 ;;
esac

case "$(( $REPLY % 2 ))" in
    0)
	{
	    dd if=/usr/share/OVMF/OVMF_CODE_4M.fd of=pcode.bin 
	    dd if=/usr/share/OVMF/OVMF_VARS_4M.fd of=pvars.bin
	} 2>/dev/null
	BOOT="
	-drive if=pflash,readonly=on,format=raw,file=pcode.bin
	-drive if=pflash,readonly=off,format=raw,file=pvars.bin
	"
	;;
    1)
	BOOT="${BOOT--boot menu=on,splash-time=60000}"
	;;
esac

# Setup $FILE with an optional $1 appendix as FILE and FILE.o overlay, 
setup_writable() {
    echo setup_writable >&2
    if [ -z "$FUSEFILE" ] ; then
	echo "Needs writable $FILE" >&2
	exit 1
    fi
    echo "# Using fusefile overlay protection on $FILE"
    $FUSEFILE -ononempty FILE -overlay:FILE.o $FILE $1 || exit 1
    FILE=FILE
    exiting() {
	fusermount -u FILE
	rm -f FILE FILE.o
	trap "" 0
    }
    trap "exiting" 0 2 9 15
}

case "$REPLY" in
    1|2)
	ISO="-drive index=1,format=raw,media=disk,file=$FILE"
	;;
    3|4)
	ISO="-drive index=1,format=raw,media=cdrom,file=$FILE"
	;;
    5|6)
	USBHOSTS=(
	    # ich9-usb-ehci1 # too slow
	    # ich9-usb-ehci2 # cpage out of range (5)
	    # ich9-usb-uhci1 # too slow
	    # ich9-usb-uhci2 # too slow
	    #ich9-usb-uhci3
	    #ich9-usb-uhci4
	    #ich9-usb-uhci5
	    #ich9-usb-uhci6
	    nec-usb-xhci # ok
	    pci-ohci # ok, slow
	    # piix3-usb-uhci # too slow
	    # piix4-usb-uhci # too slow
	    qemu-xhci # ok
	    # usb-ehci # broken
	    # vt82c686b-usb-uhci # too slow
	)
	select USBHOST in ${USBHOSTS[@]} ; do
	    [ -z "$USBHOST" ] || break
	done
	ISO="
        -device ${USBHOST},id=usb0
        -drive if=none,id=stick,format=raw,media=disk,file=${FILE}
        -device usb-storage,bus=usb0.0,port=1,drive=stick
	"
	;;
    7|8)
	ISO="
 	-device nvme,id=nvme-ctrl-0,serial=beeffood
        -drive if=none,id=stick,format=raw,media=disk,file=${FILE}
	-device nvme-ns,drive=stick,bus=nvme-ctrl-0
	"
	;;
    9|10)
	# SD card device must be 2^N in size
	X=$(stat -c %s $(realpath ${FILE}))
	V=( $(echo "l($X)/l(2)" | bc -l | tr . ' ') )
	if [ ${V[1]:0:10} -ne 0 ] ; then
	    N=$(( 2**(${V[0]}+1) - $X))
	    if [ -z $FUSEFILE ] ; then
		echo "### SD card must be a power of 2 in size; add $N"
		exit 1
	    fi
	    echo "# SD card must be a power of 2 in size; adding $N zeroes"
	    setup_writable /dev/null/:$N
	fi
	ISO="
	-device sdhci-pci,id=sdhc
        -drive if=none,id=stick,format=raw,media=disk,file=${FILE}
	-device sd-card,drive=stick
	"
	;;
    11|12)
	SCSIHOSTS=(
	    # am53c974
	    dc390
	    # ich9-ahci
	    lsi53c895a
	    megasas megasas-gen2
	    # mptsas1068
	    pvscsi
	    virtio-scsi-pci
	)
	select SCSIHOST in ${SCSIHOSTS[@]} ; do
            [ -z "$SCSIHOST" ] || break
	done
	ISO="
	-device ${SCSIHOST},id=scsi0
        -drive if=none,id=stick,format=raw,media=disk,file=${FILE}
	-device scsi-hd,drive=stick,bus=scsi0.0,channel=0,scsi-id=0,lun=0
	"
	;;
esac

STAT=$(stat -Lc %a $FILE)
echo "stat = $STAT $FILE"
if [ -z "${STAT##4*}" ] || [ ! -w $FILE ] ; then
    setup_writable
    ISO="$(echo "${ISO[@]}" | sed 's/file=[^ ]*/file=FILE/')"
    ls -l FILE
fi

case "$NET" in
    vde)
	# vde mode networking requries a vde_switch set up to uplink
	# to the host via a tap. Similar to tap mode but doesn't
	# require root running qemu.
	NET="-net nic,model=e1000 -net vde,sock=/run/vde.ctl"
	;;
    user)
	# User mode networking uses pcap to attach "invisibly" to the
	# uplink host interface with a faked network 10.0.2.0/24. 
	# 
	NET="-nic user,ipv6=off"
	;;
    *)
	NET="-nic none"
	;;
esac

[ -e disk.raw ] || dd if=/dev/zero of=disk.raw bs=1G count=0 seek=20

ARGS=(
    -m 2G $MACHINE
    -serial mon:stdio -echr 0x1c # Using ^\ as meta character
    $BOOT
    -drive index=0,id=disk,media=disk,format=raw,file=disk.raw
    $ISO
    $NET
)
echo "${ARGS[*]}" | sed "s/ -/\n -/g"

$QEMU ${ARGS[@]}
