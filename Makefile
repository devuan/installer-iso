# This Makefile is for building ISOs.
# To build an X ISO, type "make ISO=X" provided there is an X.mk
# that defines the particulars. E.g. netinstall.mk
#
# The building makes use of a couple of Makefile fragments:
# boot-bios-isolinux.mk = adds BIOS/ISOLINUX boot option to the ISO tree
# boot-efi-grub.mk = adds EFI/GRUB boot option to the ISO tree
# firmware.mk = adds a "firmware links" collection to the ISO tree
# kernel.mk = adds Linux kernel and initrd to the ISO tree
# makeiso.mk = packs the ISO tree into an ISO (the final step)
#
# Note that ISO tree content also may use package pool building.
#
# Currently defined ISOs are:
# netinstall.mk = for network installation
# desktop.mk = a DVD size ISO suited for desktop
# server.mk = a CD size ISO suited for servers (no desktop)
# cd2.mk = a CD size add-on to "server" with xfce4 and ?
# cd3.mk = a CD size add-on to server with ?
# pool1.mk = a repository pool only iso
# pool1.mk = another repository pool only iso

ifeq (${DISTNAME},)
${error DISTNAME must be set}
endif
ifeq (${DISTVERSION},)
${error DISTVERSION must be set}
endif

DISTPUBLISHER = www.devuan.org
ARCH := $(shell dpkg-architecture -q DEB_HOST_ARCH)
BUILD_DATE := $(shell date +%Y%m%d)
BUILDDIR := build/${ARCH}
ISOTREE = ${BUILDDIR}/${ISO}

# Kernel version .. use the build platform
KERNELVERSION := ${shell ls -r /lib/modules | head -n 1}

PATH := ${PATH}:$(dir $(realpath $(firstword $(MAKEFILE_LIST))))scripts

$(warning ISOTREE=${ISOTREE})

include dependencies.mk

ifneq (${ISO},)

default: .dependencies ${ISO}-${ARCH}.iso

# This makes boot and pool choices, and provides the udeb list
include ${ISO}.mk

# Process udeb selection
UDEBSET =
${eval ${shell sed "s/#.*//;/^\\s*$$/d" udeb-sets.mk.tmpl > udeb-sets.mk}}
include udeb-sets.mk
UDEBLIST = ${shell scripts/expand-udeb-set.sh ${UDEBSET}}

# This provides kernel and initrd targets
include kernel.mk

# This provides targets for package collections
include packages.mk

# This provides targets for including a firmware collection
include firmware.mk

# This provides the ISO build target as well as docs and meta targets
include makeiso.mk

# This provides the capture of the package list files
include saverun.mk

else
default:
	@echo no default. use "clean" to clean
endif

# Generic target for making directories
%/:
	@mkdir -p $@

.PHONY: udeblist
udeblist:
	yes | ( cd pool ; ./review.lsp ../udebs-initial ) > udeblist
	grep UNAVALIABLE udeblist >&2

.PHONY: all
all: BUILD = netinstall server cd2 cd3 cd4 desktop pool1
LOGPFX := LOG-${ARCH}.${shell date "+%Y%m%d-%H%M%S"}
all: .dependencies
	for B in ${BUILD} ; do \
	    make ISO=$$B clean build || exit 1 ; \
	done | 2>&1 tee ${LOGPFX}

clean:
	rm -rf build/${ARCH}
	make -C pool clean

allclean:
	rm -rf build
	make -C pool clean

reallyclean: allclean
	make -C pool reallyclean

LAT = boot/isolinux/pics/devuan-daedalus-bootscreen-iso-640x480.png
boot/isolinux/pics/devuan-%-bootscreen-iso-640x480.png: $(LAT)
	ln -s $$(basename $<) $@
