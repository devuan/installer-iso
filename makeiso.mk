# This Makefile fragment defines how to pack the pieces of an ISO
# into an actual ISO image.
# env ISO
# env ISOTREE
# env DISTNAME
# env DISTVERSION
# env ARCH
# env BUILD_DATE

# DISTVERSION has format like "5.0.0-rc5"
VOLI = $(subst .,,$(word 1,$(subst -, ,DEVUAN${DISTVERSION:-%=})))
APPI = Devuan ${DISTNAME} ${DISTVERSION}

${warning ${VOLI} = ${APPI}}

ISOCONTENT = pool.${POOL} ${BOOTOPTIONS} ${FIRMWARE} ${DOCS} ${META} ${MD5SUM}

META += ${ISOTREE}/.disk/info

ifneq (${IS_INSTALLER},)
DOCS = $(subst docs/,${ISOTREE}/,$(wildcard docs/*) docs/BOOTHELP.txt)
META += ${ISOTREE}/.disk/base_installable
META += ${ISOTREE}/.disk/cd_type
META += ${ISOTREE}/debian
META += ${ISOTREE}/devuan

ifeq (${DISTNAME},beowulf)
# For beowulf there needs to be a .disk/base_include file on the ISO
# with "libelogind0" in it since nothing will pull it in by dependency

META += ${ISOTREE}/.disk/base_include
${ISOTREE}/.disk/base_include:
	echo libelogind0 > $@
endif
 # DISTNAME = beowulf

docs/BOOTHELP.txt: $(wildcard boot/isolinux/f??.txt)
docs/BOOTHELP.txt: $(wildcard boot/isolinux/f?.txt)
	cat $^ | scripts/sldoc.lsp | env -i \
	    DEBIAN_VERSION="${DISTNAME} ${DISTVERSION}" \
	    DISTNAME=${DISTNAME} \
	    BUILD_DATE=${BUILD_DATE} \
	    envsubst > $@

${DOCS}: ${ISOTREE}/%: docs/%
	cp $< $@

${ISOTREE}/.disk/cd_type:
	echo ${IS_INSTALLER} > $@
endif
 # IS_INSTALLER

MD5SUM = ${ISOTREE}/md5sum.txt
MD5SUM = MD5TMP

# Making the .disk/info file
DISKINFO = Devuan GNU/Linux ${DISTVERSION}
DISKINFO += ${DISTNAME} ${ARCH}
DISKINFO += - ${ISO} $(BUILD_DATE)
${ISOTREE}/.disk/info: ${ISOTREE}/.disk/
	echo -n "${DISKINFO}" > $@

# Making the .disk/base_installable file
${ISOTREE}/.disk/base_installable: ${ISOTREE}/.disk/
	touch $@

# Making the debian and devuan links
${ISOTREE}/debian ${ISOTREE}/devuan: ${ISOTREE}/.disk/
	ln -s . $@

# Making the disk's md5sum
MD5SUM = ${ISOTREE}/md5sum.txt
MD5TMP = MD5TMP
MD5CMD = (cd ${ISOTREE} ; (find . -type f -printf "%P\0" | xargs -0 md5sum))
.INTERMEDIATE: ${MD5TMP}
${MD5TMP}:
	${MD5CMD} > $@

${MD5SUM}: ${MD5TMP}
	sed '/isolinux.bin$$/d' ${MD5TMP} > $@

# Making installer documentation by copying from "docs/"
INSTALLGUIDESRC := ${shell find docs -type f | grep -v .gitkeep}
INSTALLGUIDE = ${patsubst docs/%,${ISOTREE}/%,${INSTALLGUIDESRC}}
${INSTALLGUIDE}: ${ISOTREE}/%: docs/%
	mkdir -p $$(dirname $@)
	cp $< $@

ifneq (${BOOTBIN},)

ifneq (${EFIIMG},)
 # Also defines EFILOADER

define CDROMEFIBOOT =
-boot_image isolinux next \
-append_partition 2 0xef ${EFIIMG} \
-boot_image any efi_path=--interval:appended_partition_2:all::${EFILOADER}
endef

else
CDROMEFIBOOT =
endif
 # has EFIIMG

define CDROMBOOT =
-boot_image isolinux system_area=/usr/lib/ISOLINUX/isohdpfx.bin \
-boot_image isolinux dir=/boot/isolinux \
-boot_image  any iso_mbr_part_type=0x00 \
-boot_image isolinux  partition_entry=gpt_basdat \
${CDROMEFIBOOT}
endef

else
CDROMBOOT =
endif
 # has BOOTBIN

${ISO}-${ARCH}.iso: ${ISOCONTENT}
	xorriso -report_about ALL \
	    -outdev $@ map ${ISOTREE} / \
	    -rockridge on -joliet on \
	    -application_id "${APPI}" \
	    -volid "${VOLI}" \
	    -volset_id "${ISO}" \
	    -publisher "${DISTPUBLISHER}" \
	    -system_id "${ISO}" \
            -preparer_id "${DISTPUBLISHER}" \
	    -uid 0 -gid 0 \
	    ${CDROMBOOT}

# Implements the "build" target
PHONY: build
build: ${ISO}-${ARCH}.iso save-run
	: # do nothing more

