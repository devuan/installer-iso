# Provide warnings about build dependencies for project installer-iso

# call: vital_dep_check(which,newlisp,Error message on failure)
define vital_dep_check
	@if test "$(1)" = "which" ; \
	then \
		which $(2) 1>/dev/null 2>&1 || \
			{ echo "Fatal! $(3) Aborted." 1>&2 ; exit 1 ; } \
	elif test "$(1)" = "wildcard" ; \
	then \
		find $(2) -maxdepth 0 -mindepth 0 1>/dev/null 2>&1 || \
			{ echo "Fatal! $(3) Aborted." 1>&2 ; exit 1 ; } \
	fi
endef

$(warning dependencies.mk: ARCH=${ARCH})

.PHONY: .dependencies
.dependencies:
	$(call vital_dep_check,which,newlisp,Install package newlisp.)
	$(call vital_dep_check,wildcard,/usr/lib/ISOLINUX/isolinux.bin,Install package isolinux for file /usr/lib/ISOLINUX/isolinux.bin)
	$(call vital_dep_check,which,reduce-font,Install package libbogl-dev for file /usr/bin/reduce-font.)
	$(call vital_dep_check,wildcard,/usr/src/unifont.bdf,Install package bf-utf-source for file /usr/src/unifont.bdf.)
ifeq (${ARCH},i386)
	$(call vital_dep_check,wildcard,/usr/lib/grub/i386-efi/moddep.lst,Install package grub-efi-ia32-bin for file /usr/lib/grub/i386-efi/moddep.lst.)
else
	$(call vital_dep_check,wildcard,/usr/lib/grub/x86_64-efi/moddep.lst,Install package grub-efi-amd64-bin for file /usr/lib/grub/x86_64-efi/moddep.lst.)
endif
	$(call vital_dep_check,wildcard,/sbin/mkfs.msdos,Install package dosfstools for file /sbin/mkfs.msdos.)
	$(call vital_dep_check,which,mmd,Install package mtools for file /usr/bin/mmd.)
	$(call vital_dep_check,wildcard,/usr/share/win32/win32-loader.exe,Install package win32-loader for file /usr/share/win32/win32-loader.exe.)
	$(call vital_dep_check,which,todos,Install package tofrodos for file /usr/bin/todos.)
	$(call vital_dep_check,which,xorriso,Install package xorriso for file /usr/bin/xorriso.)
	$(call vital_dep_check,which,isohybrid,Install package syslinux-utils for file /usr/bin/isohybrid.)
	$(call vital_dep_check,which,zsh,Install package zsh for file /usr/bin/zsh.)
	$(call vital_dep_check,which,dpkg-scanpackages,Install package dpkg-dev for file /usr/bin/dpkg-scanpackages)
	$(call vital_dep_check,which,gzip,Install package gzip for file /bin/gzip)
